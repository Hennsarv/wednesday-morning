﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            String nimi = "Henn";
            string teinenimi = "Sarv";

            int i = 7;
            double a = i;
            double b = 7.7;
            int j = (int)b;
            Console.WriteLine(j);

            byte b1 = 0; // 1baidine arv
            short s1 = b1; // kahebaidine arv
            int i1 = s1; // neljbaidine arv
            long l1 = i1; // kaheksabaidine arv
            i1 = (int)l1;
            s1 = (short)i1;

            Byte b2 = 77;
            Int16 s2 = b2;
            Int32 i2 = s2;
            Int64 l2 = i2;

            string u1 = b2.ToString("0000");

            Console.WriteLine(u1);

            double minuRaha = 7.2872345;



            Console.WriteLine(minuRaha.ToString("F2"));


            string minuArv = "77,28";
            //double minuDouble = double.Parse(minuArv);

            if (double.TryParse(minuArv, out double minuDouble))
            {
                Console.WriteLine("Arv on {0}", minuDouble);
            }
            else Console.WriteLine("see pole miski arv");

            int ii = 777; byte bb = (byte)ii; // = 9 
            Console.WriteLine(bb);

            // need neli rida teevad sama asja
            Console.WriteLine("Täna on " + DateTime.Now.ToString() + " ja see on tore!");
            Console.WriteLine(String.Format("Täna on {0} ja see on tore!", DateTime.Now));
            Console.WriteLine("Täna on {0} ja see on tore!", DateTime.Now);
            Console.WriteLine($"Täna on {DateTime.Now} ja see on tore!");

            if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
            {
                Console.WriteLine("täna toome kala");
            }
            else
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                {
                    Console.WriteLine("täna toome õlut ja juustu");
                    Console.WriteLine("paneme ka sauna sooja");
                }

                else
                {
                    Console.WriteLine("täna toome liha");
                }
            }

        }
    }
}
